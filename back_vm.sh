#!/bin/sh

DATE=`date +"%Y%m%d-%H%M"`
#VM_NAME="nexus"
VM_NAME=$1
IMG_DIR="/tmp"

###################################

VM_SNAP_NAME=${VM_NAME}_snap_${DATE}
VM_SNAP_VOL_NAME=${VM_NAME}_snap_vol_${DATE}
VM_SNAP_IMG_NAME=${VM_NAME}_snap_img_${DATE}

VM_ID=`openstack server list --name ${VM_NAME} -c ID -f value`
VOL_ID=` openstack server show  ${VM_ID} -c volumes_attached -f value | awk -F\' '{print "openstack volume show -c attachments -f json "$2 " | grep -B 1 vda" }' | sh | grep volume_id |awk -F\" '{print $4}'`
###################################

echo "########################################################## "
echo "[" `date`"]" "Start vm snapshot "
openstack snapshot create --name ${VM_SNAP_NAME} ${VOL_ID} --force

echo "########################################################## "
echo "[" `date`"]" "VM snapshot information"
openstack volume  snapshot show ${VM_SNAP_NAME}

while : ; do
  VOL_SNAP_STAT=`openstack volume  snapshot show -c status -f value ${VM_SNAP_NAME}`
  echo "[" `date`"]" "Snapshot volume  uploading...( status: ${VOL_SNAP_STAT} )"
  if [ "${VOL_SNAP_STAT}" == "available" ]; then
      break
  fi
  sleep 5
done

echo "########################################################## "
echo "[" `date`"]" "Start volume create"
openstack volume create --snapshot ${VM_SNAP_NAME} ${VM_SNAP_VOL_NAME} 





echo "########################################################## "
echo "[" `date`"]" "VM snapshot volume information"
openstack volume show ${VM_SNAP_VOL_NAME}




while : ; do
  VOL_STAT=`openstack volume  show -c status -f value ${VM_SNAP_VOL_NAME}`
  echo "[" `date`"]" "Snapshot volume  uploading...( status: ${VOL_STAT} )"
  if [ "${VOL_STAT}" == "available" ]; then
      break
  fi
  sleep 5
done


echo "[" `date`"]" "Upload volume to image"
cinder upload-to-image --disk-format raw ${VM_SNAP_VOL_NAME}  ${VM_SNAP_IMG_NAME}

echo "[" `date`"]" "Image information"
cinder show ${VM_SNAP_IMG_NAME}


while : ; do
  IMG_STAT=`openstack image show -c status -f value ${VM_SNAP_IMG_NAME}`
  echo "[" `date`"]" "Cinder image uploading...( status: ${IMG_STAT} )"
  if [ "${IMG_STAT}" == "active" ]; then
      break
  fi
  sleep 5
done

echo  "[" `date`"]" "Delete vm snapshot "
openstack volume snapshot delete ${VM_SNAP_NAME}

echo  "[" `date`"]" "Delete vm snapshot volume"
openstack volume delete ${VM_SNAP_VOL_NAME}

echo  "[" `date`"]" "Download Image"
openstack image save --file ${IMG_DIR}/${VM_SNAP_IMG_NAME}.raw ${VM_SNAP_IMG_NAME}

echo  "[" `date`"]" "Delete Image"
openstack image delete ${VM_SNAP_IMG_NAME}

echo  "[" `date`"]" "Delete vm snapshot "
openstack volume snapshot delete ${VM_SNAP_NAME}

